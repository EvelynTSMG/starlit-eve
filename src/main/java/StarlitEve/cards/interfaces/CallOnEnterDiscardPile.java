package StarlitEve.cards.interfaces;

import com.megacrit.cardcrawl.cards.AbstractCard;

public interface CallOnEnterDiscardPile {
	void onEnterDiscardPile(AbstractCard card);
}
