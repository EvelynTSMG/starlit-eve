package StarlitEve.cards.interfaces;

import com.megacrit.cardcrawl.cards.AbstractCard;

public interface OnChargeCard {
	void onCharge(int chargeAmount);
}
