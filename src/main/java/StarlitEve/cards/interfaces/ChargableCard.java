package StarlitEve.cards.interfaces;

public interface ChargableCard {
	void charge(int amount);
	void resetCharge();
}
