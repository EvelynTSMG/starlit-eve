package StarlitEve.cards.interfaces;

import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public interface CallAfterUse {
	void afterUse(AbstractCard card, AbstractMonster m);
}
