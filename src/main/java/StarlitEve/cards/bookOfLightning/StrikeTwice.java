package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cardmods.ChargeStatInfo;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class StrikeTwice extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(StrikeTwice.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 1;
	private static final int DMG = 6;
	private static final int DMG_STEP = 5;
	private static final int DMG_MULT = 2;
	private static final int UP_DMG_MULT = 3;
	private static final int CHARGE_MAX = 2;

	public StrikeTwice() {
		super(ID, COST, TYPE, RARITY, TARGET);

		baseDamage = damage = DMG;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();

		CardModifierManager.addModifier(this,
				new ChargeMod(COST, CHARGE_MAX, new ChargeStatInfo(ChargeStatInfo.StatType.DAMAGE, DMG_STEP)));
	}

	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		for (int i = 0; i < (upgraded ? UP_DMG_MULT : DMG_MULT); i++) {
			Wiz.atb(new DamageAction(m,
					new DamageInfo(p, damage, DamageInfo.DamageType.NORMAL),
					AbstractGameAction.AttackEffect.LIGHTNING));
		}
	}

	@Override
	public void upp() {
		rawDescription = cardStrings.UPGRADE_DESCRIPTION;
		initializeDescription();
	}

	@Override
	public void upgradeName() {
		++timesUpgraded;
		upgraded = true;
		name = cardStrings.EXTENDED_DESCRIPTION[0];
		initializeTitle();
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.UNCOMMON;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
