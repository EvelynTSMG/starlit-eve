package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;
import com.megacrit.cardcrawl.powers.LoseDexterityPower;

public class AgileStep extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(AgileStep.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 0;
	private static final int BLK = 3;
	private static final int UP_BLK = 2;
	private static final int EFFECT = 2;
	private static final int UP_EFFECT = 1;

	public AgileStep() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseBlock = block = BLK;
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		blck();
		Wiz.applyToSelf(new DexterityPower(p, magicNumber));
		Wiz.applyToSelf(new LoseDexterityPower(p, magicNumber));
	}

	public void upp() {
		upgradeBlock(UP_BLK);
		upgradeMagicNumber(UP_EFFECT);
	}

	@Override
	public void loadImage() {
		loadCardImg("AgileStep");
	}

	static {
		RARITY = CardRarity.COMMON;
		TARGET = CardTarget.SELF;
		TYPE = CardType.SKILL;
	}
}
