package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.actions.EnergyOnKillAction;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.EnergizedPower;
import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;

public class EnergyThief extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(EnergyThief.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 2;
	private static final int DMG = 20;
	private static final int UP_DMG = 10;
	private static final int EFFECT = 1;
	private static final int UP_EFFECT = 1;

	public EnergyThief() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseDamage = damage = DMG;
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.atb(new EnergyOnKillAction(m,
					new DamageInfo(p, damage, DamageInfo.DamageType.NORMAL),
					new FlashAtkImgEffect(m.hb_x, m.hb_y, AbstractGameAction.AttackEffect.SLASH_DIAGONAL),
					(monster) -> Wiz.atb(new GainEnergyAction(magicNumber))));
		Wiz.applyToSelf(new EnergizedPower(p, magicNumber));
	}

	@Override
	public void upp() {
		upgradeDamage(UP_DMG);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.RARE;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
