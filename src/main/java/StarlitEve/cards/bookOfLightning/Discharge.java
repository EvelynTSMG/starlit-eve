package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cardmods.ChargeStatInfo;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.WeakPower;

public class Discharge extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(Discharge.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 0;
	private static final int DMG = 8;
	private static final int DMG_STEP = 4;
	private static final int EFFECT = 1;
	private static final int EFFECT_STEP = 1;
	private static final int CHARGE_MAX = 1;

	public Discharge() {
		super(ID, COST, TYPE, RARITY, TARGET);

		baseDamage = damage = DMG;
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();

		CardModifierManager.addModifier(this,
				new ChargeMod(COST, CHARGE_MAX,
						new ChargeStatInfo(ChargeStatInfo.StatType.DAMAGE, DMG_STEP),
						new ChargeStatInfo(ChargeStatInfo.StatType.MAGIC, EFFECT_STEP)));
	}

	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		dmg(m, AbstractGameAction.AttackEffect.LIGHTNING);

		if (upgraded) {
			Wiz.applyToEnemy(m, new WeakPower(m, magicNumber, false));
		}
	}

	@Override
	public void upp() {
		rawDescription = cardStrings.UPGRADE_DESCRIPTION;
		initializeDescription();
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.COMMON;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
