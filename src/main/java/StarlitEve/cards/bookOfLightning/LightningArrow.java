package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cardmods.ChargeStatInfo;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import basemod.helpers.CardModifierManager;
import com.evacipated.cardcrawl.mod.stslib.actions.common.DamageCallbackAction;
import com.evacipated.cardcrawl.mod.stslib.powers.StunMonsterPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class LightningArrow extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(LightningArrow.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 0;
	private static final int DMG = 10;
	private static final int UP_DMG = 4;
	private static final int DMG_STEP = 6;
	private static final int UP_DMG_STEP = 8;
	private static final int CHARGE_MAX = 4;

	public LightningArrow() {
		super(ID, COST, TYPE, RARITY, TARGET);

		baseDamage = damage = DMG;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		tags.add(Starlight.util.CustomTags.STARLIGHT_ARROW);
		loadImage();

		CardModifierManager.addModifier(this,
				new ChargeMod(COST, CHARGE_MAX,
						new ChargeStatInfo(ChargeStatInfo.StatType.DAMAGE, DMG_STEP, UP_DMG_STEP)));
	}

	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.atb(new DamageCallbackAction(m, new DamageInfo(p, damage, damageTypeForTurn),
				AbstractGameAction.AttackEffect.SLASH_HORIZONTAL, (unblockedDamage) -> {
			if (unblockedDamage > m.maxHealth/2) {
				Wiz.applyToEnemy(m, new StunMonsterPower(m));
			}
		}));
	}

	@Override
	public void upp() {
		upgradeDamage(UP_DMG);
	}

	@Override
	public void loadImage() {
		loadCardImg("LightningArrow");
	}

	static {
		RARITY = CardRarity.COMMON;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
