package StarlitEve.cards.bookOfLightning;

import StarlitEve.StarlitEve;
import Starlight.util.Wiz;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;

public class Paralyze extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(Paralyze.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 0;
	private static final int EFFECT = 1;
	private static final int UP_EFFECT = 1;

	public Paralyze() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.applyToEnemy(m, new WeakPower(m, magicNumber, false));
		Wiz.applyToEnemy(m, new VulnerablePower(m, magicNumber, false));
	}

	public void upp() {
		upgradeMagicNumber(UP_EFFECT);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.COMMON;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.SKILL;
	}
}
