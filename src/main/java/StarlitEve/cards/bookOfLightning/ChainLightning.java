package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.actions.TransDamageAction;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cardmods.ChargeStatInfo;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;

public class ChainLightning extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(ChainLightning.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 1;
	private static final int DMG = 13;
	private static final int UP_DMG = 5;
	private static final int DMG_STEP = 4;
	private static final int UP_DMG_STEP = 6;
	private static final int EFFECT = 1;
	private static final int UP_EFFECT = 1;
	private static final int CHARGE_MAX = 2;

	public ChainLightning() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseDamage = damage = DMG;
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();

		CardModifierManager.addModifier(this,
				new ChargeMod(COST, CHARGE_MAX,
						new ChargeStatInfo(ChargeStatInfo.StatType.DAMAGE, DMG_STEP, UP_DMG_STEP)));
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.atb(new TransDamageAction(m,
				new DamageInfo(p, damage, DamageInfo.DamageType.NORMAL),
				AbstractGameAction.AttackEffect.LIGHTNING));
		Wiz.applyToEnemy(m, new VulnerablePower(m, magicNumber, false));
	}

	public void upp() {
		upgradeDamage(UP_DMG);
		upgradeMagicNumber(UP_EFFECT);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.UNCOMMON;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
