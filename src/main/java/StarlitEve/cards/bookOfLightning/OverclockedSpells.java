package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.powers.OverclockedSpellsPower;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class OverclockedSpells extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(OverclockedSpells.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 1;
	private static final int EFFECT = 1;
	private static final int UP_EFFECT = 1;

	public OverclockedSpells() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}


	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.applyToSelf(new OverclockedSpellsPower(p, magicNumber, AbstractDungeon.player.hand));
	}

	@Override
	public void upp() {
		upgradeMagicNumber(UP_EFFECT);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.UNCOMMON;
		TARGET = CardTarget.SELF;
		TYPE = CardType.POWER;
	}
}
