package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cardmods.ChargeStatInfo;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.ThornsPower;

public class ElectroShield extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(ElectroShield.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 0;
	private static final int BLOCK = 6;
	private static final int UP_BLOCK = 2;
	private static final int BLOCK_STEP = 6;
	private static final int UP_BLOCK_STEP = 8;
	private static final int EFFECT = 1;
	private static final int UP_EFFECT = 1;
	private static final int EFFECT_STEP = 1;
	private static final int UP_EFFECT_STEP = 2;
	private static final int CHARGE_MAX = 2;

	public ElectroShield() {
		super(ID, COST, TYPE, RARITY, TARGET);

		baseBlock = block = BLOCK;
		baseMagicNumber = magicNumber = EFFECT;
		exhaust = true;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();

		CardModifierManager.addModifier(this,
				new ChargeMod(COST, CHARGE_MAX,
						new ChargeStatInfo(ChargeStatInfo.StatType.BLOCK, BLOCK_STEP, UP_BLOCK_STEP),
						new ChargeStatInfo(ChargeStatInfo.StatType.MAGIC, EFFECT_STEP, UP_EFFECT_STEP)));
	}

	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		blck();
		Wiz.applyToSelf(new ThornsPower(p, magicNumber));
	}

	@Override
	public void upp() {
		upgradeBlock(UP_BLOCK);
		upgradeMagicNumber(UP_EFFECT);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.RARE;
		TARGET = CardTarget.SELF;
		TYPE = CardType.SKILL;
	}
}