package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.actions.TransDamageAction;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;

public class Lightningbolt extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(Lightningbolt.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 1;
	private static final int DMG = 9;
	private static final int UP_DMG = 7;

	public Lightningbolt() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseDamage = damage = DMG;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.atb(new TransDamageAction(m, new DamageInfo(p, damage, damageTypeForTurn),
				AbstractGameAction.AttackEffect.LIGHTNING));
	}

	public void upp() {
		upgradeDamage(UP_DMG);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.COMMON;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
