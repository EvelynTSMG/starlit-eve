package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cardmods.ChargeStatInfo;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.EnergizedPower;
import com.megacrit.cardcrawl.powers.WeakPower;

public class EnergyReserve extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(EnergyReserve.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 0;
	private static final int EFFECT = 1;
	private static final int EFFECT_STEP = 2;
	private static final int EFFECT_2 = 1;
	private static final int CHARGE_MAX = 2;

	public EnergyReserve() {
		super(ID, COST, TYPE, RARITY, TARGET);

		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();

		CardModifierManager.addModifier(this,
				new ChargeMod(COST, CHARGE_MAX,
						new ChargeStatInfo(ChargeStatInfo.StatType.MAGIC, EFFECT_STEP)));
	}

	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.applyToSelf(new EnergizedPower(p, magicNumber));

		if (!upgraded) {
			Wiz.applyToSelf(new WeakPower(p, EFFECT_2, false));
		}
	}

	@Override
	public void upp() {
		rawDescription = cardStrings.UPGRADE_DESCRIPTION;
		initializeDescription();
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.UNCOMMON;
		TARGET = CardTarget.SELF;
		TYPE = CardType.SKILL;
	}
}
