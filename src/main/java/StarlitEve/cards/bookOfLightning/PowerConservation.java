package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.powers.PowerConservationPower;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

public class PowerConservation extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(PowerConservation.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 2;
	private static final int EFFECT = 1;

	public PowerConservation() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseMagicNumber = magicNumber = EFFECT;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}


	@Override
	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.applyToSelf(new PowerConservationPower(p, magicNumber));
	}

	@Override
	public void upp() {
		selfRetain = true;
		rawDescription = cardStrings.UPGRADE_DESCRIPTION;
		initializeDescription();
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.RARE;
		TARGET = CardTarget.SELF;
		TYPE = CardType.POWER;
	}
}
