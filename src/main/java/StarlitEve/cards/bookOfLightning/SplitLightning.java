package StarlitEve.cards.bookOfLightning;

import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.WeakPower;

public class SplitLightning extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(SplitLightning.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = 1;
	private static final int DMG = 7;
	private static final int UP_DMG = 5;
	private static final int EFFECT = 1;

	public SplitLightning() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseDamage = damage = DMG;
		baseMagicNumber = magicNumber = EFFECT;
		isMultiDamage = true;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		allDmg(AbstractGameAction.AttackEffect.LIGHTNING);
		Wiz.forAllMonstersLiving((monster) -> Wiz.applyToEnemy(monster, new WeakPower(monster, magicNumber, false)));
	}

	public void upp() {
		upgradeDamage(UP_DMG);
	}

	@Override
	public void loadImage() {
		loadCardImg("SplitLightning");
	}

	static {
		RARITY = CardRarity.UNCOMMON;
		TARGET = CardTarget.ALL_ENEMY;
		TYPE = CardType.ATTACK;
	}
}
