package StarlitEve.cards.bookOfLightning;

import Starlight.actions.EasyXCostAction;
import Starlight.util.Wiz;
import StarlitEve.StarlitEve;
import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.util.CustomTags;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerToRandomEnemyAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;

public class LightningDance extends AbstractCustomMagickCard {
	public static final String ID = StarlitEve.makeId(LightningDance.class.getSimpleName());
	private static final AbstractCard.CardRarity RARITY;
	private static final AbstractCard.CardTarget TARGET;
	private static final AbstractCard.CardType TYPE;
	private static final int COST = -1;
	private static final int DMG = 9;
	private static final int UP_DMG = 4;
	private static final int EFFECT = 1;
	private static final int UP_EFFECT = 1;

	public LightningDance() {
		super(ID, COST, TYPE, RARITY, TARGET);
		baseDamage = damage = DMG;
		baseMagicNumber = magicNumber = EFFECT;
		isMultiDamage = true;
		tags.add(CustomTags.STARLIGHT_LIGHTNING);
		loadImage();
	}

	public void use(AbstractPlayer p, AbstractMonster m) {
		Wiz.atb(new EasyXCostAction(this, (effect, params) -> {
			int x = effect;
			for (int i = 0; i < x; i++) {
				Wiz.atb(new DamageAction(m,
						new DamageInfo(p, damage, DamageInfo.DamageType.NORMAL),
						AbstractGameAction.AttackEffect.LIGHTNING));
			}

			for (int i = 0; i < x; i++) {
				Wiz.atb(new ApplyPowerToRandomEnemyAction(p,
						new VulnerablePower(null, 1, false),
						1));
			}

			return true;
		}));
	}

	public void upp() {
		upgradeDamage(UP_DMG);
		upgradeMagicNumber(UP_EFFECT);
	}

	@Override
	public void loadImage() {
		loadCardImg("Paralyze");
	}

	static {
		RARITY = CardRarity.RARE;
		TARGET = CardTarget.ENEMY;
		TYPE = CardType.ATTACK;
	}
}
