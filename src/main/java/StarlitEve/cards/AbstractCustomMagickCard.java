package StarlitEve.cards;

import Starlight.cards.abstracts.AbstractEasyCard;
import Starlight.cards.abstracts.AbstractMagickCard;
import Starlight.util.TexLoader;
import StarlitEve.util.CustomTags;
import StarlitEve.util.HelperFuns;
import basemod.ReflectionHacks;
import basemod.abstracts.CustomCard;
import basemod.helpers.CardTags;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.cards.AbstractCard;
import javafx.util.Pair;

import java.util.Map;

public abstract class AbstractCustomMagickCard extends AbstractMagickCard {
	public static String resFolderName;

	public AbstractCustomMagickCard(String cardId, int cost, AbstractCard.CardType type, AbstractCard.CardRarity rarity, AbstractCard.CardTarget target) {
		super(cardId, cost, type, rarity, target);
		ReflectionHacks.setPrivate(this, AbstractEasyCard.class, "needsArtRefresh", false);
	}

	public AbstractCustomMagickCard(String cardId, int cost, AbstractCard.CardType type, AbstractCard.CardRarity rarity, AbstractCard.CardTarget target, AbstractCard.CardColor color) {
		super(cardId, cost, type, rarity, target, color);
		ReflectionHacks.setPrivate(this, AbstractEasyCard.class, "needsArtRefresh", false);
	}

	protected void loadCardImg(String cardName) {
		textureImg = resFolderName + "/images/cards/" + cardName + ".png";

		CustomCard.imgMap.remove(textureImg);

		loadCardImage(textureImg);
	}

	@Override
	public TextureAtlas.AtlasRegion getSpellbookIcon() {
		for (Map.Entry<CardTags, String> entry : CustomTags.BOOK_NAMES.entrySet()) {
			if (hasTag(entry.getKey())) {
				return TexLoader.getTextureAsAtlasRegion(HelperFuns.getVanillaBookImagePath(entry.getValue()));
			}
		}

		return null;
	}

	public abstract void loadImage();
}
