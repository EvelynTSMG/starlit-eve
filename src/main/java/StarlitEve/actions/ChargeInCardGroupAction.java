package StarlitEve.actions;

import StarlitEve.StarlitEve;
import Starlight.actions.BetterSelectCardsCenteredAction;
import Starlight.actions.BetterSelectCardsInHandAction;
import Starlight.util.Wiz;
import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cards.interfaces.ChargableCard;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import java.util.ArrayList;
import java.util.function.Predicate;

public class ChargeInCardGroupAction extends AbstractGameAction {
	private static final String[] TEXT;
	private static final float DUR;
	private final CardGroup cardGroup;
	private final Predicate<AbstractCard> filter;
	private final AbstractGameAction followUpAction;
	private final boolean canPickZero;
	private final boolean anyNumber;
	private final int chargeAmount;

	public ChargeInCardGroupAction(CardGroup cardGroup) {
		this(cardGroup, 1);
	}
	public ChargeInCardGroupAction(CardGroup cardGroup, int chargeAmount) {
		this(cardGroup, chargeAmount, 1);
	}

	public ChargeInCardGroupAction(CardGroup cardGroup, int chargeAmount, int amount) {
		this(cardGroup, chargeAmount, amount, true);
	}

	public ChargeInCardGroupAction(CardGroup cardGroup, int chargeAmount, int amount, boolean canPickZero) {
		this(cardGroup, chargeAmount, amount, canPickZero, (c) -> {
			if (CardModifierManager.hasModifier(c, ChargeMod.ID)) {
				ChargeMod chargeMod = (ChargeMod)CardModifierManager.getModifiers(c, ChargeMod.ID).get(0);
				return chargeMod.chargeStep < chargeMod.chargeMax;
			}

			return false;
		});
	}

	public ChargeInCardGroupAction(CardGroup cardGroup, int chargeAmount, int amount, boolean canPickZero,
								   Predicate<AbstractCard> filter) {
		this(cardGroup, chargeAmount, amount, canPickZero, filter, null);
	}

	public ChargeInCardGroupAction(CardGroup cardGroup, int chargeAmount, int amount, boolean canPickZero,
								   Predicate<AbstractCard> filter, AbstractGameAction followUpAction) {
		this.cardGroup = cardGroup;
		this.amount = amount;
		anyNumber = amount == -1;
		this.canPickZero = canPickZero;
		this.filter = filter;
		actionType = AbstractGameAction.ActionType.CARD_MANIPULATION;
		duration = DUR;
		this.followUpAction = followUpAction;
		this.chargeAmount = chargeAmount;
	}

	public void update() {
		if (AbstractDungeon.getCurrRoom().isBattleEnding() || amount == 0) {
			isDone = true;
		}

		ArrayList<AbstractCard> validCards = new ArrayList<>();
		cardGroup.group.stream().filter(filter).forEach(validCards::add);

		if (!validCards.isEmpty()) {
			if (amount > validCards.size()) {
				amount = validCards.size();
			}

			String tip = TEXT[0]; // "Charge"

			if (cardGroup == Wiz.adp().hand) {

				Wiz.att(new BetterSelectCardsInHandAction(amount, tip, anyNumber, canPickZero, validCards::contains,
						(cards) -> {
					for (AbstractCard card : cards) {
						if (CardModifierManager.hasModifier(card, ChargeMod.ID)) {
							((ChargeMod)CardModifierManager.getModifiers(card, ChargeMod.ID).get(0)).charge(card, 1);
						}
					}
				}));
			} else {
				Wiz.att(new BetterSelectCardsCenteredAction(validCards, amount, tip, anyNumber, !canPickZero,
						(cards) -> {
					for (AbstractCard card : cards) {
						if (CardModifierManager.hasModifier(card, ChargeMod.ID)) {
							((ChargeMod)CardModifierManager.getModifiers(card, ChargeMod.ID).get(0)).charge(card, 1);
						}
					}
				}));
			}

			if (followUpAction != null) {
				addToTop(followUpAction);
			}
		}

		isDone = true;
	}

	static {
		TEXT = CardCrawlGame.languagePack.getUIString(StarlitEve.makeId(ChargeInCardGroupAction.class.getSimpleName())).TEXT;
		DUR = Settings.ACTION_DUR_FASTER;
	}
}
