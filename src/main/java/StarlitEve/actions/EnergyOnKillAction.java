package StarlitEve.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.vfx.AbstractGameEffect;

import java.util.function.Consumer;

public class EnergyOnKillAction extends AbstractGameAction {
	private final DamageInfo info;
	private final Consumer<AbstractCreature> callback;
	private final AbstractGameEffect effect;

    public EnergyOnKillAction(AbstractCreature target, DamageInfo info, AbstractGameEffect attackEffect, Consumer<AbstractCreature> callback) {
		this.info = info;
		setValues(target, info);
		actionType = ActionType.DAMAGE;
		duration = Settings.ACTION_DUR_FASTER;
		effect = attackEffect;
		this.callback = callback;
	}

	@Override
	public void update() {
		if (duration == Settings.ACTION_DUR_FASTER && target != null) {
			AbstractDungeon.effectList.add(effect);
			target.damage(info);
			if (target.isDying || target.currentHealth <= 0) {
				callback.accept(target);
			}
			isDone = true;

			if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
				AbstractDungeon.actionManager.clearPostCombatActions();
			}
		}

		tickDuration();
	}
}
