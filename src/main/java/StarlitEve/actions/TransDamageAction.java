package StarlitEve.actions;

import Starlight.util.Wiz;
import com.evacipated.cardcrawl.mod.stslib.actions.common.DamageCallbackAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

public class TransDamageAction extends AbstractGameAction {
	private final DamageInfo info;
	private final AttackEffect effect;

	public TransDamageAction(AbstractCreature target, DamageInfo info, AttackEffect attackEffect) {
		this.info = info;
		setValues(target, info);
		actionType = ActionType.DAMAGE;
		duration = Settings.ACTION_DUR_FASTER;
		effect = attackEffect;
	}

	@Override
	public void update() {
		if (duration == Settings.ACTION_DUR_FASTER && target != null) {
			int oldHp = target.currentHealth;
			Wiz.att(new DamageCallbackAction(target, info, effect, (unblockedDamage) -> {
				if (unblockedDamage >= oldHp) {
					target = AbstractDungeon
							.getMonsters()
							.getRandomMonster(null, true, AbstractDungeon.cardRandomRng);
					Wiz.att(new DamageAction(target, info, effect));
				}
			}));

			isDone = true;
			if (AbstractDungeon.getCurrRoom().monsters.areMonstersBasicallyDead()) {
				AbstractDungeon.actionManager.clearPostCombatActions();
			}
		}

		tickDuration();
	}
}
