package StarlitEve.actions;

import StarlitEve.cardmods.ChargeMod;
import StarlitEve.cards.interfaces.ChargableCard;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

public class ChargeAction  extends AbstractGameAction {
	private static final float DUR;
	private final AbstractCard card;
	private final int amount;

	public ChargeAction(AbstractCard card) {
		this(card, 1);
	}

	public ChargeAction(AbstractCard card, int amount) {
		this.card = card;
		this.amount = amount;
		actionType = ActionType.SPECIAL;
		duration = DUR;
	}

	public void update() {
		if (AbstractDungeon.getCurrRoom().isBattleEnding() || card == null || amount == 0) {
			isDone = true;
		}

		if (CardModifierManager.hasModifier(card, ChargeMod.ID)) {
			((ChargeMod)CardModifierManager.getModifiers(card, ChargeMod.ID).get(0)).charge(card, 1);
		}

		isDone = true;
	}

	static {
		DUR = Settings.ACTION_DUR_FASTER;
	}
}
