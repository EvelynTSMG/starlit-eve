package StarlitEve;

import StarlitEve.cards.AbstractCustomMagickCard;
import StarlitEve.powers.AbstractCustomEasyPower;
import basemod.AutoAdd;
import basemod.BaseMod;
import basemod.interfaces.EditCardsSubscriber;
import basemod.interfaces.EditKeywordsSubscriber;
import basemod.interfaces.EditStringsSubscriber;
import basemod.interfaces.PostInitializeSubscriber;
import com.badlogic.gdx.Gdx;
import com.evacipated.cardcrawl.mod.stslib.Keyword;
import com.evacipated.cardcrawl.modthespire.lib.SpireInitializer;
import com.google.gson.Gson;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.localization.UIStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;

@SpireInitializer
public class StarlitEve implements
		EditCardsSubscriber,
		EditStringsSubscriber,
		EditKeywordsSubscriber,
		PostInitializeSubscriber {
	public static final Logger logger = LogManager.getLogger(StarlitEve.class.getName());
	private static final String modId = "StarlitEve";
	private static final String resFolderName = "Girlcock";

	// In-game mod settings panel
	private static final String MODNAME = "Starlit Eve";
	private static final String AUTHOR = "StarlitEve";
	private static final String DESCRIPTION = "An addon for The Starlight Sisters by Mistress Alison.";

	public StarlitEve() {
		BaseMod.subscribe(this);
	}

	public static void initialize() {
		logger.info("========== Initializing Starlit Eve ==========");
		StarlitEve starlitEve = new StarlitEve();

		logger.info("=========| Initialized: Starlit Eve |=========");
	}

	public static String makeId(String idText) {
		return modId + ":" + idText;
	}

	private void loadLocalizedStrings(Class<?> stringClass, String fileName) {
		BaseMod.loadCustomStringsFile(stringClass, resFolderName + "/localization/eng/" + fileName + ".json");
		if (!Settings.language.toString().equalsIgnoreCase("eng")) {
			String path = resFolderName + "/localization/" + Settings.language.toString().toLowerCase() + "/" + fileName + ".json";
			if (Gdx.files.internal(path).exists()) {
				BaseMod.loadCustomStringsFile(stringClass, path);
			}
		}

	}

	@Override
	public void receiveEditCards() {
		// Set resFolderName for custom stuffs
		AbstractCustomMagickCard.resFolderName = resFolderName;

		new AutoAdd(modId).packageFilter("StarlitEve.cards").setDefaultSeen(true).cards();
	}

	@Override
	public void receiveEditStrings() {
		AbstractCustomEasyPower.resFolderName = resFolderName;

		loadLocalizedStrings(CardStrings.class, "Cards");
		loadLocalizedStrings(UIStrings.class, "UI");
		loadLocalizedStrings(PowerStrings.class, "Powers");
	}

	@Override
	public void receiveEditKeywords() {
		Gson gson = new Gson();
		String json = Gdx.files.internal(resFolderName + "/localization/eng/Keywords.json").readString(String.valueOf(StandardCharsets.UTF_8));
		Keyword[] keywords = gson.fromJson(json, Keyword[].class);
		if (keywords != null) {
			for(Keyword keyword : keywords) {
				BaseMod.addKeyword("StarlitEve".toLowerCase(), keyword.PROPER_NAME, keyword.NAMES, keyword.DESCRIPTION);
			}
		}
	}

	@Override
	public void receivePostInitialize() {
		// Load card images
		AutoAdd autoImg = new AutoAdd(modId).packageFilter("StarlitEve.cards");
		autoImg.any(AbstractCard.class, (info, card) -> {
			if (card instanceof AbstractCustomMagickCard) {
				((AbstractCustomMagickCard)card).loadImage();
			}
		});

		// Auto add powers
		(new AutoAdd(modId)).packageFilter("StarlitEve.powers").any(AbstractPower.class, (info, power) -> {
			BaseMod.addPower(power.getClass(), power.ID);
		});
	}
}