package StarlitEve.util;

import Starlight.util.TexLoader;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.megacrit.cardcrawl.cards.AbstractCard;
import javassist.CtBehavior;

import java.util.*;

public class CustomTags {
	@SpireEnum
	public static AbstractCard.CardTags STARLIGHT_LIGHTNING;

	public static final Map<AbstractCard.CardTags, String> BOOK_NAMES = new HashMap<>();

	static {
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_WATER, "BookWater");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_ICE, "BookIce");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_FIRE, "BookFire");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_NATURE, "BookNature");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_DARK, "BookDark");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_LIGHT, "BookLight");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_SPACE, "BookSpace");
		BOOK_NAMES.put(Starlight.util.CustomTags.STARLIGHT_TIME, "BookTime");
		BOOK_NAMES.put(CustomTags.STARLIGHT_LIGHTNING, "BookElec");
	}

	public static class Locator extends SpireInsertLocator {
		@Override
		public int[] Locate(CtBehavior ctBehavior) throws Exception {
			return LineFinder.findInOrder(ctBehavior, new Matcher.MethodCallMatcher(ArrayList.class, "add"));
		}
	}

	@SpirePatch2(clz = Starlight.util.CustomTags.class, method = "getMagicTags")
	public static class AddCustomTags {
		@SpireInsertPatch(locator = Locator.class, localvars = {"l"})
		public static void addTags(ArrayList<AbstractCard.CardTags> l) {
			l.add(STARLIGHT_LIGHTNING);
		}
	}
}
