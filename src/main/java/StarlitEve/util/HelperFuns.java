package StarlitEve.util;

import StarlitEve.StarlitEve;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.core.CardCrawlGame;

public class HelperFuns {
	private static final String[] CARD_GROUP_TYPE_TEXT;

	public static String CardGroupTypeToString(CardGroup group) {
		if (group == null) {
			return CARD_GROUP_TYPE_TEXT[6];
		}

		switch (group.type) {
			case DRAW_PILE: return CARD_GROUP_TYPE_TEXT[0];
			case MASTER_DECK: return CARD_GROUP_TYPE_TEXT[1];
			case HAND: return CARD_GROUP_TYPE_TEXT[2];
			case DISCARD_PILE: return CARD_GROUP_TYPE_TEXT[3];
			case EXHAUST_PILE: return CARD_GROUP_TYPE_TEXT[4];
			case CARD_POOL: return CARD_GROUP_TYPE_TEXT[5];
			default: case UNSPECIFIED: return CARD_GROUP_TYPE_TEXT[6];
		}
	}


	public static String getVanillaBookImagePath(String bookName) {
		return "StarlightResources/images/books/" + bookName + ".png";
	}

	static {
		CARD_GROUP_TYPE_TEXT = CardCrawlGame.languagePack.getUIString(StarlitEve.makeId("CardGroupType")).TEXT;
	}
}
