package StarlitEve.cardmods;

import StarlitEve.cards.interfaces.CallAfterUse;
import StarlitEve.cards.interfaces.CallOnEnterDiscardPile;
import StarlitEve.powers.interfaces.OnChargePower;
import basemod.ReflectionHacks;
import basemod.abstracts.AbstractCardModifier;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class ChargeMod extends AbstractCardModifier implements CallAfterUse, CallOnEnterDiscardPile {
	public static final String ID = StarlitEve.StarlitEve.makeId(ChargeMod.class.getSimpleName());
	public final ArrayList<ChargeStatInfo> statInfo = new ArrayList<>();
	protected boolean justPlayed = false;
	protected final int COST;
	public int chargeStep = 0;
	public int baseChargeMax;
	public int chargeMax;

	public ChargeMod(int cost, int chargeMax, ChargeStatInfo... infos) {
		this(cost, chargeMax, new ArrayList<>(Arrays.asList(infos)));
	}

	private ChargeMod(int cost, int chargeMax, ArrayList<ChargeStatInfo> infos) {
		COST = cost;
		baseChargeMax = this.chargeMax = chargeMax;
		statInfo.addAll(infos);
	}

	@Override
	public float modifyBaseDamage(float damage, DamageInfo.DamageType type, AbstractCard card, AbstractMonster monster) {
		float bonus = 0;
		for (ChargeStatInfo info : statInfo) {
			if (info.stat == ChargeStatInfo.StatType.DAMAGE) {
				bonus = (card.upgraded ? info.up_step : info.step) * chargeStep;
			}
		}
		return damage + bonus;
	}

	@Override
	public float modifyBaseBlock(float block, AbstractCard card) {
		float bonus = 0;
		for (ChargeStatInfo info : statInfo) {
			if (info.stat == ChargeStatInfo.StatType.BLOCK) {
				bonus = (card.upgraded ? info.up_step : info.step) * chargeStep;
			}
		}
		return block + bonus;
	}

	@Override
	public float modifyBaseMagic(float magic, AbstractCard card) {
		float bonus = 0;
		for (ChargeStatInfo info : statInfo) {
			if (info.stat == ChargeStatInfo.StatType.MAGIC) {
				bonus = (card.upgraded ? info.up_step : info.step) * chargeStep;
			}
		}
		return magic + bonus;
	}

	@Override
	public String modifyDescription(String rawDesc, AbstractCard card) {
		String color = chargeMax > baseChargeMax ? "#g" : chargeMax < baseChargeMax ? "#r" : "";
		return rawDesc.replaceAll("!StarlitEve:ChargeMax!", color + chargeMax);
	}

	@Override
	public boolean isInherent(AbstractCard card) {
		return true;
	}

	@Override
	public AbstractCardModifier makeCopy() {
		ArrayList<ChargeStatInfo> new_infos = new ArrayList<>();
		for (ChargeStatInfo inf : statInfo) {
			new_infos.add(new ChargeStatInfo(inf));
		}
		return new ChargeMod(COST, chargeMax, new_infos);
	}

	@Override
	public String identifier(AbstractCard card) {
		return ID;
	}

	@Override
	public void onUse(AbstractCard card, AbstractCreature target, UseCardAction action) {
		justPlayed = true; // =)
	}

	@Override
	public void afterUse(AbstractCard card, AbstractMonster monster) {
		resetCharge(card); // Reset cost only *after* energy is consumed!
	}

	public void onEnterDiscardPile(AbstractCard card) {
		charge(card, 1);
	}


	public void charge(AbstractCard card, int amount) {
		if (justPlayed) {
			justPlayed = false;
			return;
		}

		if (chargeStep != chargeMax) {
			chargeStep += amount;
			chargeStep = Math.max(0, Math.min(chargeStep, chargeMax));

			card.costForTurn = card.cost = COST + chargeStep;
			CardModifierManager.testBaseValues(card);
			card.initializeDescription();
		} else {
			// this is for OnCharge stuff to be able to expect amount to be the difference, save for clamping
			amount = 0;
		}

		ArrayList<AbstractPower> allPowers = new ArrayList<>();
		allPowers.addAll(AbstractDungeon
				.getCurrRoom()
				.monsters.monsters.stream()
				.map((m) -> m.powers)
				.reduce(
						new ArrayList<>(),
						(result, next) -> {
							result.addAll(next); return result;
						}));
		allPowers.addAll(AbstractDungeon.player.powers);

		for (AbstractPower p : allPowers) {
			if (p instanceof OnChargePower) {
				((OnChargePower) p).onCharge(card, amount);
			}
		}
	}

	public void resetCharge(AbstractCard card) {
		chargeStep = 0;
		card.costForTurn = card.cost = COST;
		card.resetAttributes();
		CardModifierManager.testBaseValues(card);
		card.initializeDescription();
	}
}
