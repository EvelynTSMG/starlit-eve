package StarlitEve.cardmods;

public class ChargeStatInfo {
	public StatType stat;
	public int step;
	public int up_step;

	public ChargeStatInfo(StatType stat, int step) {
		this(stat, step, step);
	}

	public ChargeStatInfo(StatType stat, int step, int up_step) {
		this.stat = stat;
		this.step = step;
		this.up_step = up_step;
	}

	public ChargeStatInfo(ChargeStatInfo info) {
		stat = info.stat;
		step = info.step;
		up_step = info.up_step;
	}

	public enum StatType {
		DAMAGE,
		BLOCK,
		MAGIC
	}
}
