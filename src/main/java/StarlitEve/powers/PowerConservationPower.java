package StarlitEve.powers;

import StarlitEve.StarlitEve;
import StarlitEve.cardmods.ChargeMod;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

public class PowerConservationPower extends AbstractCustomEasyPower {
	public static final String NAME;
	public static final String[] TEXT;

	public PowerConservationPower() {
		this(AbstractDungeon.player, 1);
	}

	public PowerConservationPower(AbstractCreature owner, int amount) {
		super(NAME, "PowerConservation",
				PowerType.BUFF, false, owner, amount);
		ID = StarlitEve.makeId(PowerConservationPower.class.getSimpleName());
		this.owner = owner;
		this.amount = amount;
	}

	@Override
	public void atEndOfTurnPreEndTurnCards(boolean isPlayer) {
		if (isPlayer) {
			for (AbstractCard c : AbstractDungeon.player.hand.group) {
				if (CardModifierManager.hasModifier(c, ChargeMod.ID)) {
					ChargeMod mod = (ChargeMod)CardModifierManager.getModifiers(c, ChargeMod.ID).get(0);
					if (mod.chargeStep == mod.chargeMax) {
						c.retain = true;
					}
				}
			}
		}
	}

	@Override
	public void updateDescription() {
		description = TEXT[0];
		switch (amount) {
			case 1: description += TEXT[1]; break;
			case 2: description += TEXT[2]; break;
			case 3: description += TEXT[3]; break;
			default: description += amount + TEXT[4]; break;
		}
		description += TEXT[5];
	}

	static {
		NAME = CardCrawlGame.languagePack.getPowerStrings(StarlitEve.makeId("PowerConservationPower")).NAME;
		TEXT = CardCrawlGame.languagePack.getPowerStrings(StarlitEve.makeId("PowerConservationPower")).DESCRIPTIONS;
	}
}
