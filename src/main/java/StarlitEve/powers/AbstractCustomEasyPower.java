package StarlitEve.powers;

import Starlight.util.TexLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.powers.AbstractPower;

public abstract class AbstractCustomEasyPower extends AbstractPower {
	public static String resFolderName;

	public AbstractCustomEasyPower(String name, String imgName, AbstractPower.PowerType powerType, boolean isTurnBased, AbstractCreature owner, int amount) {
		this.isTurnBased = isTurnBased;
		this.name = name;
		this.owner = owner;
		this.amount = amount;
		type = powerType;

		Texture normalTexture = TexLoader.getTexture(resFolderName + "/images/powers/" + imgName + "32.png");
		Texture hiDefImage = TexLoader.getTexture(resFolderName + "/images/powers/" + imgName + "84.png");
		if (hiDefImage != null) {
			region128 = new TextureAtlas.AtlasRegion(hiDefImage, 0, 0, hiDefImage.getWidth(), hiDefImage.getHeight());
			if (normalTexture != null) {
				region48 = new TextureAtlas.AtlasRegion(normalTexture, 0, 0, normalTexture.getWidth(), normalTexture.getHeight());
			}
		} else if (normalTexture != null) {
			img = normalTexture;
			region48 = new TextureAtlas.AtlasRegion(normalTexture, 0, 0, normalTexture.getWidth(), normalTexture.getHeight());
		} else {
			StarlitEve.StarlitEve.logger.info("Failed to find images at "
					+ resFolderName + "/images/powers/" + imgName + "32.png"
					+ " | "
					+ resFolderName + "/images/powers/" + imgName + "84.png");
		}

		updateDescription();
	}
}
