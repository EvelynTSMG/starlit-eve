package StarlitEve.powers;

import StarlitEve.StarlitEve;
import Starlight.util.Wiz;
import StarlitEve.actions.ChargeInCardGroupAction;
import StarlitEve.util.HelperFuns;
import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class OverclockedSpellsPower extends AbstractCustomEasyPower implements NonStackablePower {
	public static final String NAME;
	public static final String[] TEXT;
	private final CardGroup TARGET;

	public OverclockedSpellsPower() {
		this(AbstractDungeon.player, 1,
				CardCrawlGame.isInARun() ?
						AbstractDungeon.player.hand : new CardGroup(CardGroup.CardGroupType.UNSPECIFIED));
	}

	public OverclockedSpellsPower(AbstractCreature owner, int amount) {
		this(owner, amount,
				CardCrawlGame.isInARun() ?
						AbstractDungeon.player.hand : new CardGroup(CardGroup.CardGroupType.UNSPECIFIED));
	}

	public OverclockedSpellsPower(AbstractCreature owner, int amount, CardGroup target) {
		super(NAME, "OverclockedSpells",
				PowerType.BUFF, true, owner, amount);
		ID = StarlitEve.makeId(OverclockedSpellsPower.class.getSimpleName());
		TARGET = target;
		updateDescription();
	}

	@Override
	public void atStartOfTurnPostDraw() {
		Wiz.atb(new ChargeInCardGroupAction(TARGET, 1, amount));
	}

	@Override
	public void updateDescription() {
		if (amount == 1) {
			description = TEXT[1] + HelperFuns.CardGroupTypeToString(TARGET) + TEXT[4];
		} else {
			description = TEXT[2] + amount + TEXT[3] + HelperFuns.CardGroupTypeToString(TARGET) + TEXT[4];
		}
	}

	@Override
	public boolean isStackable(AbstractPower power) {
		return power instanceof OverclockedSpellsPower && ((OverclockedSpellsPower) power).TARGET == TARGET;
	}

	static {
		NAME = CardCrawlGame.languagePack.getPowerStrings(StarlitEve.makeId("OverclockedSpellsPower")).NAME;
		TEXT = CardCrawlGame.languagePack.getPowerStrings(StarlitEve.makeId("OverclockedSpellsPower")).DESCRIPTIONS;
	}
}
