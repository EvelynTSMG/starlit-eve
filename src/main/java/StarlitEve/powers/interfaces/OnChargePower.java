package StarlitEve.powers.interfaces;

import com.megacrit.cardcrawl.cards.AbstractCard;

public interface OnChargePower {
	void onCharge(AbstractCard card, int chargeAmount);
}
