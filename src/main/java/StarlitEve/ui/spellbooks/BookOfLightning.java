package StarlitEve.ui.spellbooks;

import StarlitEve.StarlitEve;
import StarlitEve.cards.bookOfLightning.Paralyze;
import StarlitEve.util.CustomTags;
import Starlight.cards.abstracts.AbstractAbilityCard;
import Starlight.powers.abilities.EquinoxPower;
import Starlight.ui.spellbooks.ClickableSpellbook;
import Starlight.util.TexLoader;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class BookOfLightning extends ClickableSpellbook {
	public static final String[] TEXT;
	public static final String internalName = "BookElec";

	public BookOfLightning(boolean prim) {
		super(TexLoader.getTextureAsAtlasRegion("StarlightResources/images/books/" + internalName + ".png"),
				TEXT[0], TEXT[1],
				new AbstractAbilityCard(new EquinoxPower((AbstractCreature)null, 0, prim)) {
		});
	}
	@Override
	public String starterCardID() {
		return Paralyze.ID;
	}

	@Override
	public boolean allowCardInPool(AbstractCard card) {
		return card.hasTag(CustomTags.STARLIGHT_LIGHTNING);
	}

	@Override
	public AbstractPower getAbility(boolean b) {
		return null;
	}

	static {
		TEXT = CardCrawlGame.languagePack.getUIString(StarlitEve.makeId("BookOfLightning")).TEXT;
	}
}
