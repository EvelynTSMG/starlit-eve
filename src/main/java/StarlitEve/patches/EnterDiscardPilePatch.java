package StarlitEve.patches;

import Starlight.util.Wiz;
import StarlitEve.cards.interfaces.CallOnEnterDiscardPile;
import basemod.abstracts.AbstractCardModifier;
import basemod.helpers.CardModifierManager;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatches2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePostfixPatch;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;

@SpirePatches2({
	@SpirePatch2(clz = CardGroup.class, method = "addToTop"),
	@SpirePatch2(clz = CardGroup.class, method = "addToBottom"),
	@SpirePatch2(clz = CardGroup.class, method = "addToRandomSpot")
})
public class EnterDiscardPilePatch {
	@SpirePostfixPatch
	public static void postAddToDiscard(CardGroup __instance, AbstractCard c) {
		if (Wiz.adp() != null && __instance.equals(Wiz.adp().discardPile)) {
			for (AbstractCardModifier mod : CardModifierManager.modifiers(c)) {
				if (mod instanceof CallOnEnterDiscardPile) {
					((CallOnEnterDiscardPile) mod).onEnterDiscardPile(c);
				}
			}
		}
	}
}
