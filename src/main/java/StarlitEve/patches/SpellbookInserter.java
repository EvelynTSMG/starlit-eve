package StarlitEve.patches;

import StarlitEve.StarlitEve;
import Starlight.patches.CompendiumPatches;
import Starlight.ui.*;
import Starlight.util.TexLoader;
import StarlitEve.ui.spellbooks.BookOfLightning;
import StarlitEve.util.CustomTags;
import basemod.ReflectionHacks;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import javassist.CtBehavior;

import static StarlitEve.util.HelperFuns.getVanillaBookImagePath;

public class SpellbookInserter {
	public static class Locator1 extends SpireInsertLocator {
		@Override
		public int[] Locate(CtBehavior ctBehavior) throws Exception {
			return LineFinder.findInOrder(ctBehavior, new Matcher.MethodCallMatcher(SpellbookPanel.class, "layoutBooks"));
		}
	}

	@SpirePatch2(clz = SpellbookUI.class, method = SpirePatch.CONSTRUCTOR)
	public static class SpellbookInsert {
		@SpireInsertPatch(locator = Locator1.class)
		public static void addSpellbooks(SpellbookUI __instance) {
			SpellbookPanel prim = ReflectionHacks.getPrivate(__instance, SpellbookUI.class, "primPanel");
			SpellbookPanel luna = ReflectionHacks.getPrivate(__instance, SpellbookUI.class, "lunaPanel");

			prim.addBook(new BookOfLightning(true));
			luna.addBook(new BookOfLightning(false));
		}
	}

	public static class Locator2 extends SpireInsertLocator {
		@Override
		public int[] Locate(CtBehavior ctBehavior) throws Exception {
			return LineFinder.findInOrder(ctBehavior, new Matcher.MethodCallMatcher(FilterPanel.class, "layoutFilters"));
		}
	}

	@SpirePatch2(clz = FilterUI.class, method = SpirePatch.CONSTRUCTOR)
	public static class FilterInsert {
		public static final String[] TEXT;

		@SpireInsertPatch(locator = Locator2.class, localvars = { "group", "size" })
		public static void addSpellbookFilters(FilterUI __instance, CardGroup group, int size) {
			FilterPanel filterPanel = ReflectionHacks.getPrivate(__instance, FilterUI.class, "filterPanel");

			filterPanel.addFilter(getBookFilter(__instance, group, size,
					0, getVanillaBookImagePath(BookOfLightning.internalName), CustomTags.STARLIGHT_LIGHTNING));
		}

		private static ClickableFilter getBookFilter(FilterUI __instance, CardGroup group,
													 int size, int textIdx, String imagePath,
													 AbstractCard.CardTags tag) {
			return new ClickableFilter(
					TexLoader.getTextureAsAtlasRegion(imagePath),
					TEXT[textIdx], __instance.makeBody(__instance.countByPred(
					group, (c) -> c.hasTag(tag)), size)) {
				public void onSelect() {
					CompendiumPatches.cardFilter = (c) -> c.hasTag(tag);
					__instance.refreshFilters();
				}
			};
		}

		static {
			TEXT = CardCrawlGame.languagePack.getUIString(StarlitEve.makeId("FilterUI")).TEXT;
		}
	}
}
