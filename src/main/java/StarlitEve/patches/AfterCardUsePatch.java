package StarlitEve.patches;

import StarlitEve.cards.interfaces.CallAfterUse;
import StarlitEve.cards.interfaces.CallOnEnterDiscardPile;
import basemod.abstracts.AbstractCardModifier;
import basemod.helpers.CardModifierManager;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePostfixPatch;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

@SpirePatch2(clz = AbstractPlayer.class, method = "useCard")
public class AfterCardUsePatch {
	@SpirePostfixPatch
	public static void postUseCard(AbstractCard c, AbstractMonster monster) {
		for (AbstractCardModifier mod : CardModifierManager.modifiers(c)) {
			if (mod instanceof CallAfterUse) {
				((CallAfterUse) mod).afterUse(c, monster);
			}
		}
	}
}